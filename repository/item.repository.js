const itemModel = require('../model').Item;

async function createItems(newItems) {
    return itemModel.bulkCreate(newItems, {returning: true});
}

async function getAllItems() {
    return itemModel.findAll();
}

async function getItemById(itemId) {
    return itemModel.findByPk(itemId);
}

module.exports = {
    createItems,
    getAllItems,
    getItemById
}