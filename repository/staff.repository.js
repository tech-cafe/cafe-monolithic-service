const staffModel = require('../model').Staff;

async function createStaff(newStaff) {
    return staffModel.create(newStaff);
}

async function getStaffByIdCardNo(staffIdCardNo) {
    return staffModel.findByPk(staffIdCardNo);
}

async function getAllStaffs() {
    return staffModel.findAll();
}

async function getStaffByUsername(username) {
    return staffModel.findOne({ where: {username: username} });
}

async function getStaffByEmail(email) {
    return staffModel.findOne({ where: {email: email} });
}

module.exports = {
    createStaff,
    getStaffByIdCardNo,
    getStaffByUsername,
    getAllStaffs,
    getStaffByEmail
}