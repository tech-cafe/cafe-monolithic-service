const addItemModel = require('../model').AddItem;

async function createAdditionalItems(newItems) {
    return addItemModel.bulkCreate(newItems, { returning: true });
}

async function getAllAdditionalItems() {
    return addItemModel.findAll();
}

async function getAdditionalItemById(addItemId) {
    return addItemModel.findByPk(addItemId);
}

async function modifyAdditionalItemById(addItemId, newValue) {
    const additionalItem = await addItemModel.findByPk(addItemId);
    return additionalItem.update(newValue);
}

async function deleteAdditionalItemById(addItemId) {
    const additionalItem = await addItemModel.findByPk(addItemId);
    return additionalItem.destroy();

}

async function getAssociatedItems(addItemId) {
    const additionalItem = await addItemModel.findByPk(addItemId);
    return additionalItem.getItems();
}

async function unassociatWithItems(addItemId, itemIds) {
    const additionalItem = await addItemModel.findByPk(addItemId);
    return additionalItem.removeItems(itemIds, { returning: true });

}

module.exports = {
    createAdditionalItems,
    getAllAdditionalItems,
    getAdditionalItemById,
    modifyAdditionalItemById,
    deleteAdditionalItemById,
    getAssociatedItems,
    unassociatWithItems
}