const itemRepository = require('./item.repository');
const staffRepository = require('./staff.repository');

module.exports = {
    itemRepository,
    staffRepository
}