const itemRoute = require('./item.route');
const billRoute = require('./bill.route');
const userRoute = require('./customer.route');
const addItemRoute = require('./add-item.route');
const staffRoute = require('./staff.route');

module.exports = {
  itemRoute,
  billRoute,
  userRoute,
  addItemRoute,
  staffRoute
};
