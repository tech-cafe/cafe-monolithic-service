const express = require('express');
const router = express.Router();
const itemService = require('../service/item.service');
const { logger } = require('../config/app.config');
const { authenticateJWT } = require('../config/passport.config');

router.get('/items', async (req, res, next) => {
  try {
    res.json(await itemService.getAllItems());
  } catch (error) {
    logger.error('Error when getting all items');
    next(error);
  }
});

router.get('/items/:itemId', async (req, res, next) => {
  const itemId = req.params.itemId;
  try {
    res.json(await itemService.getItemById(itemId));
  } catch (error) {
    logger.error(`Error when getting item with id ${itemId}`);
    next(error);
  }
});

router.post('/items', authenticateJWT(), async (req, res, next) => {
  try {
    res.json(await itemService.createItems(req.body));
  } catch (error) {
    logger.error(`Error when creating items  ${req.body}`);
    next(error);
  }
});

module.exports = router;
