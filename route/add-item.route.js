const express = require('express');
const router = express.Router();
const additionalItemService = require("../service/add-item.service");
const { logger } = require('../config/app.config');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.send('order service');
});

router.get('/addItem', async (req, res, next) => {
  try {
    res.json(await additionalItemService.getAllAdditionalItem());
  }
  catch (error) {
    logger.error(`Cannot get additional item`);
    next(error);
  }
});

router.post('/addItem', async (req, res, next) => {
  try {
    res.json(await additionalItemService.addAdditionalItems(req.body));
  }
  catch (error) {
    logger.error(`Cannot add additional Item with data ${JSON.stringify(req.body)}`);
    next(error);
  }
});

router.put('/addItem/:addItemId', async (req, res, next) => {
  try {
    res.json(await additionalItemService.modifyAdditionalItem(req.params.addItemId, req.body));
  }
  catch (error) {
    if (error instanceof TypeError) {
      // Unexisting additional item
      res.status(404).json(`Additional item ${req.params.addItemId} does not exist`);
    }
    else {
      logger.error(`Cannot modify additional item ${req.params.addItemId} with new value ${req.body}`);
      next(error);
    }
  }
})

router.delete('/addItem/:addItemId', async (req, res, next) => {
  try {
    res.json(await additionalItemService.deleteAdditionalItem(req.params.addItemId));
  }
  catch (error) {
    if (error instanceof TypeError) {
      // Unexisting additional item
      res.status(404).json(`Additional item ${req.params.addItemId} does not exist`);
    }
    else {
      logger.error(`Cannot delete additional item ${req.params.addItemId}`);
      next(error);
    }
  }
})

router.get('/addItem/:addItemId/item', async (req, res, next) => {
  try {
    res.json(await additionalItemService.getAllAssociatedItems(req.params.addItemId));
  }
  catch (error) {
    logger.error(`Cannot get item of additional item ${req.params.addItemId}`);
    next(error);
  }
})

router.post('/addItem/:addItemId/item', async (req, res, next) => {
  try {
    res.json(await additionalItemService.associateWithItems(req.params.addItemId, req.body.itemIds));
  }
  catch (error) {
    if (error instanceof TypeError) {
      // Unexisting additional item
      res.status(404).json(`Additional item ${req.params.addItemId} does not exist`);
    }
    else {
      logger.error(`Cannot associate additional item ${req.params.addItemId} with item ${req.body.itemId}`);
      next(error);
    }
  }
});

router.delete('/addItem/:addItemId/item', async (req, res, next) => {
  try {
    res.json(await additionalItemService.unassociateWithItems(req.params.addItemId, req.body.itemIds));
  }
  catch (error) {
    if (error instanceof TypeError) {
      // Unexisting additional item
      res.status(404).json(`Additional item ${req.params.addItemId} does not exist`);
    }
    else {
      logger.error(`Cannot unassociate the additional item ${req.params.addItemId} with items ${req.body.itemIds}`);
      next(error);
    }
  }
})

module.exports = router;
