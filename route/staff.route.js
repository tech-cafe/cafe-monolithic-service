const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const staffService = require('../service/staff.service');
const { logger } = require('../config/app.config');
const { authenticateJWT, passport, JWT_SECRET_KEY } = require('../config/passport.config');
const JWTExpirationMs = process.env.JWT_EXPIRATION_MS || 604800000; // milliseconds: 7 days

router.post('/staffs', async (req, res, next) => {
    try {
        res.json(await staffService.createStaff(req.body));
    } catch (error) {
        logger.error(`Error when creating staff with id card no: ${req.body.idCardNo}, username ${req.body.username}`);
        next(error);
    }
});

router.get('/staffs', authenticateJWT(), async (req, res, next) => {
    try {
        res.json(await staffService.getAllStaffs());
    } catch (error) {
        logger.error(`Error when getting all staffs. Message: ${error.message}`);
        next(error);
    }
})

router.get('/staffs/:idCardNo', authenticateJWT(), async (req, res, next) => {
    const staffIdCardNo = req.params.idCardNo;
    try {
        res.json(await staffService.getStaffByIdCardNo(staffIdCardNo));
    } catch (error) {
        logger.error(`Error when getting staff with id card no ${staffIdCardNo}. Message: ${error.message}`);
        next(error);
    }
})

router.post('/login', function (req, res, next) {
    passport.authenticate('local', {
        session: false
    }, (error, staff) => {

        if (error || !staff) {
            error.status = 401;
            return next(error);
        }

        /** This is what ends up in our JWT */
        const payload = {
            username: staff.username,
            phoneNumber: staff.phoneNumber,
            fullName: staff.fullName,
            expires: Date.now() + parseInt(JWTExpirationMs),
        };

        /** assigns payload to req.user */
        req.login(payload, {
            session: false
        }, (error) => {
            if (error) {
                error.status = 401;
                return next(error);
            }

            /** generate a signed json web token and return it in the response */
            const token = jwt.sign(JSON.stringify(payload), JWT_SECRET_KEY);

            res.status(200).json({
                token,
                message: 'Login success.'
            });
        });
        
    })(req, res, next);
});

module.exports = router;