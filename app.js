const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const passportConfig = require('./config/passport.config');
const routes = require('./route');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// add routes middleware
Object.values(routes).forEach(route => {
  app.use(route);
});

// passport authentication config
app.use(passportConfig.passport.initialize());
passportConfig.passportLocalConfig();
passportConfig.passportJWTConfig();

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  console.log(err); // TODO: log stacktrace
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  
  res.status(err.status || 500);
  res.json({
      error: err.message
    });
});

module.exports = app;
