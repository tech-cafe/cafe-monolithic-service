const billRepository = require('../model').Bill;

async function createBills(newBill) {
    return await billRepository.bulkCreate(newBill);
}

async function getBillById(billId) {
    return await billRepository.findByPk(billId);
}

async function getAllBills() {
    return await billRepository.findAll();
}

module.exports = {
    getAllBills,
    getBillById,
    createBills
} 