const addItemRepository = require("../repository/add-item.repository");
const itemRepository = require("../repository/item.repository");

async function addAdditionalItems(newAddItems) {
    return addItemRepository.createAdditionalItems(newAddItems);
}

async function getAdditionalItemById(addItemId) {
    return addItemRepository.getAdditionalItemById(addItemId)
}

async function getAllAdditionalItem() {
    return addItemRepository.getAllAdditionalItems();
}

async function modifyAdditionalItem(addItemId, newValue) {
    return addItemRepository.modifyAdditionalItemById(addItemId, newValue);
}

async function deleteAdditionalItem(addItemId) {
    return addItemRepository.deleteAdditionalItemById(addItemId);
}

async function associateWithItems(addItemId, itemIds) {
    const additionalItem = await addItemRepository.getAdditionalItemById(addItemId);
    let result = []
    await Promise.all(itemIds.map(async (itemId) => {
        const item = await itemRepository.getItemById(itemId);
        result.push(additionalItem.addItem(item));
    }));
    return result;
}

async function getAllAssociatedItems(addItemId) {
    return addItemRepository.getAssociatedItems(addItemId);
}

async function unassociateWithItems(addItemId, itemIds) {
    return addItemRepository.unassociatWithItems(addItemId, itemIds);
}

module.exports = {
    addAdditionalItems,
    getAdditionalItemById,
    getAllAdditionalItem,
    modifyAdditionalItem,
    deleteAdditionalItem,
    associateWithItems,
    getAllAssociatedItems,
    unassociateWithItems,
} 