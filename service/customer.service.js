const customerRepository = require('../model').Customer;

async function createCustomers(newCustomers) {
    return await customerRepository.bulkCreate(newCustomers);
}

async function getCustomerById(customerId) {
    return await customerRepository.findByPk(customerId);
}

async function getAllCustomers() {
    return await customerRepository.findAll();
}

module.exports = {
    getAllCustomers,
    getCustomerById,
    createCustomers
} 