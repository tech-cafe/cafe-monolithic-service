const itemService = require('./item.service');
const billService = require('./bill.service');
const userService = require('./customer.service');
const addItemService = require('./add-item.service');
const staffService = require('./staff.service');

module.exports = {
    itemService,
    billService,
    userService,
    addItemService,
    staffService
}