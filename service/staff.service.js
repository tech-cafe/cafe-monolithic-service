const bcrypt = require('bcrypt');
const passportJWT = require('passport-jwt');
const ExtractJwt = passportJWT.ExtractJwt;
const Strategy = passportJWT.Strategy;
const jwt = require('jsonwebtoken');
const { logger } = require('../config/app.config');
const { staffRepository } = require('../repository');
const StaffDto = require('../dto/staff.dto');

const SALT_ROUND = 10;

async function createStaff(newStaff) {
    await checkExistedStaff(newStaff);
    const hashedPassword = await bcrypt.hash(newStaff.password, SALT_ROUND);
    newStaff.password = hashedPassword;
    const fullNewStaffInfo = await staffRepository.createStaff(newStaff);
    return new StaffDto(
        fullNewStaffInfo.username, 
        fullNewStaffInfo.fullName, 
        fullNewStaffInfo.phoneNumber
    );
}

async function getStaffByIdCardNo(staffIdCardNo) {
    return staffRepository.getStaffByIdCardNo(staffIdCardNo);
}

async function getAllStaffs() {
    return staffRepository.getAllStaffs();
}

async function getStaffByUsername(username) {
    return staffRepository.getStaffByUsername(username);
}

async function getStaffByEmail(email) {
    return staffRepository.getStaffByEmail(email);
}

async function checkExistedStaff(newStaff) {
    var existedStaff = await getStaffByUsername(newStaff.username);

    if (existedStaff) {
        throw new Error(`Staff with username ${newStaff.username} is existed.`)
    }

    existedStaff = await getStaffByIdCardNo(newStaff.idCardNo);
    
    if (existedStaff) {
        throw new Error(`Staff with id card no ${newStaff.idCardNo} is existed.`)
    }

    existedStaff = await getStaffByEmail(newStaff.email);
    
    if (existedStaff) {
        throw new Error(`Staff with email ${newStaff.email} is existed.`)
    }
}

module.exports = {
    getAllStaffs,
    getStaffByIdCardNo,
    createStaff,
    getStaffByUsername,
    getStaffByEmail
}