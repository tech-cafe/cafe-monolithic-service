const itemRepository = require('../repository/item.repository');

async function createItems(newItems) {
    return itemRepository.createItems(newItems);
}

async function getAllItems() {
    return itemRepository.getAllItems();
}

async function getItemById(itemId) {
    return itemRepository.getItemById(itemId);
}

module.exports = {
    getAllItems,
    createItems,
    getItemById
} 