
module.exports = {
    development: {
      username: 'postgres',
      password: '123',
      database: 'tech_cafe',
      logging: false,
      host: 'localhost',
      dialect: 'postgres'
    },
    test: {
      dialect: "sqlite",
      storage: ":memory:"
    },
    production: {
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      host: process.env.DB_HOSTNAME,
      logging: false,
      dialect: 'mysql',
      use_env_variable: 'DATABASE_URL'
    }
  };