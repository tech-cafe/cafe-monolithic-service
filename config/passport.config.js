const passport = require('passport');
const passportJWT = require('passport-jwt');
const passportLocal = require('passport-local');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const staffService = require('../service/staff.service');

const JWTStrategy = passportJWT.Strategy;
const ExtractJwt = passportJWT.ExtractJwt;
const LocalStrategy = passportLocal.Strategy;

const JWT_SECRET_KEY = process.env.SECRET || 'SmartCoffee@2018!';
const opts = {
  secretOrKey: JWT_SECRET_KEY,
  jwtFromRequest: ExtractJwt.fromHeader('token')
}

function passportLocalConfig() {
    passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
    }, async (username, password, done) => {
        try {
            const staffInfo = await staffService.getStaffByUsername(username);
            const passwordsMatch = await bcrypt.compare(password, staffInfo.password);

            if (passwordsMatch) {
                return done(null, staffInfo);
            } else {
                return done(new Error('Incorrect username / password.'));
            }
        } catch (error) {
            done(error);
        }
    }));
}

function passportJWTConfig() {
    passport.use(new JWTStrategy(opts, (payload, done) => {
            if (Date.now() > payload.expires) {
                return done(new Error('JWT expired.'));
            }
            return done(null, payload);
        })
    );
}

function verifyJWTToken(token) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, JWT_SECRET_KEY, (err, decodedToken) => {
        if (err || !decodedToken) {
            return reject(err)
        }
        resolve(decodedToken)
    })
  })
}

/**
 * local strategy authenticate
 * 
 * @param {*} callback (err, userInfo) => {}
 */
function authenticateLocal(callback) {
    return passport.authenticate('local', {
        session: false
    }, callback);
}

function authenticateJWT() {
    return passport.authenticate('jwt', {
        session: false
    });
}

module.exports = {
    passport,
    authenticateJWT,
    authenticateLocal,
    passportJWTConfig,
    passportLocalConfig,
    JWT_SECRET_KEY
}
