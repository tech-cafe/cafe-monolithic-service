module.exports = class StaffDto {
    constructor(username, fullName, phoneNumber) {
        this.username = username;
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
    }
}
