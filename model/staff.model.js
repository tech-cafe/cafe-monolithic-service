'use strict';

module.exports = (sequelize, DataTypes) => {
  var Staff = sequelize.define('Staff', {
    idCardNo: {
      type: DataTypes.STRING,
      primaryKey: true,
      allowNull: false
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phoneNumber: {
      type: DataTypes.STRING,
      allowNull: false
    },
    fullName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    joinDate: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'staffs',
    underscored: false
  });

  Staff.associate = (models) => {};

  return Staff;
};
