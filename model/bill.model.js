'use strict';

module.exports = (sequelize, DataTypes) => {
  var Bill = sequelize.define('Bill', {
    billId: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    items: {
      type: DataTypes.JSONB,
      allowNull: false
    },
    tableId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    cashierId: {
      type: DataTypes.STRING,
      allowNull: false
    },
    paidTime: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'bills',
    underscored: false
  });

  Bill.associate = (models) => {
    models.Bill.belongsTo(models.Customer, {onDelete: "CASCADE"});
  };

  return Bill;
};


