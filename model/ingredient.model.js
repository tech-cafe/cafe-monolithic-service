'use strict';

module.exports = (sequelize, DataTypes) => {
  var Ingredient = sequelize.define('Ingredient', {
    ingredientId: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: false
    }
  }, {
    tableName: 'ingredients',
    underscored: false
  });

  Ingredient.associate = (models) => {
    models.Ingredient.belongsToMany(models.Recipient, {through: 'ingredients_recipients', foreignKey: 'ingredientId', onDelete: "CASCADE"});
  };

  return Ingredient;
};


