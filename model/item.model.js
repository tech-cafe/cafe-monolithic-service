'use strict';

module.exports = (sequelize, DataTypes) => {
  var Item = sequelize.define('Item', {
    itemId: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: false
    }
  }, {
    tableName: 'items',
    underscored: false
  });

  Item.associate = (models) => {
    models.Item.belongsToMany(models.AddItem, {through: 'items_addItems', foreignKey: 'itemId', onDelete: "CASCADE"});
    models.Item.hasMany(models.Recipient, {onDelete: "CASCADE"});
  };

  return Item;
};


