'use strict';

module.exports = (sequelize, DataTypes) => {
  var AddItem = sequelize.define('AddItem', {
    addItemId: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: false
    }
  }, {
    tableName: 'add_items',
    underscored: false
  });

  AddItem.associate = (models) => {
    models.AddItem.belongsToMany(models.Item, {through: 'items_addItems', foreignKey: 'addItemId', onDelete: "CASCADE"});
  };

  return AddItem;
};