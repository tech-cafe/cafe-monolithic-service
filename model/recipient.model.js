'use strict';

module.exports = (sequelize, DataTypes) => {
  var Recipient = sequelize.define('Recipient', {
    recipientId: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    content: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    tableName: 'recipients',
    underscored: false
  });

  Recipient.associate = (models) => {
    models.Recipient.belongsTo(models.Item, {onDelete: "CASCADE"});
    models.Recipient.belongsToMany(models.Ingredient, {through: 'ingredients_recipients', foreignKey: 'recipientId', onDelete: "CASCADE"});
  };

  return Recipient;
};


