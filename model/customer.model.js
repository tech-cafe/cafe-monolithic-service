'use strict';

module.exports = (sequelize, DataTypes) => {
  var Customer = sequelize.define('Customer', {
    phoneNumber: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    point: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    discountRate: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    tableName: 'customers',
    underscored: false
  });

  Customer.associate = (models) => {
    models.Customer.hasMany(models.Bill, {onDelete: "CASCADE"});
  };

  return Customer;
};
